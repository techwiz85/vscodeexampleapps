This repo contains .NET Core project examples that were created from the dotnet CLI.

The 'myconsoleapp' project was created with the following command line:
dotnet new console -o myconsoleapp

The 'mymvcapp' project was created with the following command line:
dotnet new mvc -au none -o mymvcapp
