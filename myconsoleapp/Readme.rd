﻿This file contains the docker build log file content to understand how it works.

1>------ Rebuild All started: Project: myconsoleapp, Configuration: Debug Any CPU ------
1>myconsoleapp -> C:\Code\VSCodeExampleApps\myconsoleapp\bin\Debug\netcoreapp3.1\myconsoleapp.dll
1>Docker version 19.03.5, build 633a0ea
1>docker build -f "c:\code\vscodeexampleapps\myconsoleapp\dockerfile" --force-rm -t myconsoleapp  --label "com.microsoft.created-by=visual-studio" --label "com.microsoft.visual-studio.project-name=myconsoleapp" "c:\code\vscodeexampleapps"
1>Sending build context to Docker daemon  4.402MB
1>
1>Step 1/17 : FROM mcr.microsoft.com/dotnet/core/runtime:3.1-buster-slim AS base
1>3.1-buster-slim: Pulling from dotnet/core/runtime
1>000eee12ec04: Already exists
1>67bac0b5d3cc: Already exists
1>e8c80b499c83: Already exists
1>77b73bc084ae: Already exists
1>Digest: sha256:b9a59fae6d5c7b1fe675f93f8a24fbd76638b8b7bdba9d29a73b28576b598412
1>Status: Downloaded newer image for mcr.microsoft.com/dotnet/core/runtime:3.1-buster-slim
1> ---> 08d8cf51bdfd
1>Step 2/17 : WORKDIR /app
1> ---> Running in 6f2d0971317a
1>Removing intermediate container 6f2d0971317a
1> ---> e10a0696d9fe
1>Step 3/17 : FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
1> ---> 9817c25953a8
1>Step 4/17 : WORKDIR /src
1> ---> Using cache
1> ---> 7a251d42838d
1>Step 5/17 : COPY ["myconsoleapp/myconsoleapp.csproj", "myconsoleapp/"]
1> ---> e9469a9fc6f3
1>Step 6/17 : RUN dotnet restore "myconsoleapp/myconsoleapp.csproj"
1> ---> Running in 275609acb996
1>  Restore completed in 1.37 sec for /src/myconsoleapp/myconsoleapp.csproj.
1>Removing intermediate container 275609acb996
1> ---> 3afe061e40f6
1>Step 7/17 : COPY . .
1> ---> 9ba8d66dcc7e
1>Step 8/17 : WORKDIR "/src/myconsoleapp"
1> ---> Running in d246409d8805
1>Removing intermediate container d246409d8805
1> ---> b74870f58af4
1>Step 9/17 : RUN dotnet build "myconsoleapp.csproj" -c Release -o /app/build
1> ---> Running in 44e4c2aaa76d
1>
1>Copyright (C) Microsoft Corporation. All rights reserved.
1>Microsoft (R) Build Engine version 16.4.0+e901037fe for .NET Core
1>  Restore completed in 57.45 ms for /src/myconsoleapp/myconsoleapp.csproj.
1>  myconsoleapp -> /app/build/myconsoleapp.dll
1>    0 Warning(s)
1>    0 Error(s)
1>
1>Time Elapsed 00:00:03.19
1>Build succeeded.
1>Removing intermediate container 44e4c2aaa76d
1> ---> f8c6195de1af
1>Step 10/17 : FROM build AS publish
1> ---> f8c6195de1af
1>Step 11/17 : RUN dotnet publish "myconsoleapp.csproj" -c Release -o /app/publish
1> ---> Running in 872000b8bee3
1>Microsoft (R) Build Engine version 16.4.0+e901037fe for .NET Core
1>Copyright (C) Microsoft Corporation. All rights reserved.
1>
1>  Restore completed in 61.8 ms for /src/myconsoleapp/myconsoleapp.csproj.
1>  myconsoleapp -> /src/myconsoleapp/bin/Release/netcoreapp3.1/myconsoleapp.dll
1>  myconsoleapp -> /app/publish/
1>Removing intermediate container 872000b8bee3
1> ---> 2a7c8368f2a3
1>Step 12/17 : FROM base AS final
1> ---> e10a0696d9fe
1>Step 13/17 : WORKDIR /app
1> ---> Running in 404751c19515
1>Removing intermediate container 404751c19515
1> ---> 4758053d3059
1>Step 14/17 : COPY --from=publish /app/publish .
1> ---> 8899b6f7d1ab
1>Step 15/17 : ENTRYPOINT ["dotnet", "myconsoleapp.dll"]
1> ---> Running in 51b36da8bc10
1> ---> 330ad247c4d4
1>Removing intermediate container 51b36da8bc10
1>Step 16/17 : LABEL com.microsoft.created-by=visual-studio
1> ---> Running in 143bce345ca8
1>Removing intermediate container 143bce345ca8
1> ---> b2e1baebb581
1>Step 17/17 : LABEL com.microsoft.visual-studio.project-name=myconsoleapp
1> ---> Running in 2822dd71b871
1>Removing intermediate container 2822dd71b871
1> ---> 49d1e791c73a
1>Successfully built 49d1e791c73a
1>Successfully tagged myconsoleapp:latest
1>SECURITY WARNING: You are building a Docker image from Windows against a non-Windows Docker host. All files and directories added to build context will have '-rwxr-xr-x' permissions. It is recommended to double check and reset permissions for sensitive files and directories.
========== Rebuild All: 1 succeeded, 0 failed, 0 skipped ==========
